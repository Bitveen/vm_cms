<h1>Результаты поиска</h1>
@if(count($publications) > 0)
	@foreach($publications as $publication)
		<div>
        	<h3><a href="/publications/{{$publication->id}}">{{$publication->title}}</a></h3>
			<small>{{ $publication->pub_date }}</small>
			<p>{!! mb_strcut($publication->content, 0, 300) !!}</p>
        </div>
	@endforeach
@else 
	<p>По Вашему запросу ничего не найдено.</p>
@endif