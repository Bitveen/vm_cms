<h2>Публикации в рубрике</h2>
@if(count($publications) > 0)
	@foreach($publications as $publication)
		<div>
			<h3><a href="/publications/{{ $publication->id }}">{{ $publication->title }}</a></h3>
			<small>{{ $publication->pub_date }}</small>
			<p>{!! mb_strcut($publication->content, 0, 300) !!}</p>
		</div>
	@endforeach
@else
	<p>Публикации в данной рубрике отсутствуют.</p>
@endif