@foreach($publications as $publication)
    <div class="publication">
        <h3><a href="/publications/{{$publication->id}}">{{$publication->title}}</a></h3>
        <small>{{ $publication->pub_date }}</small>
        <p>{!! mb_strcut($publication->content, 0, 300) !!}</p>
    </div>
@endforeach