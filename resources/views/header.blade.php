<header class="main-header">
	{!! $hu->body !!}
	<div class="main-header__titles">
		<h4 class="main-header__settlement-title">{{ $info->title }}</h4>
		<h3 class="main-header__settlement-name"><a href="/">{{ $info->mo_title_nominative }}</a></h3>
		<h4 class="main-header__settlement-district">{{ $info->mo_district_genitive }}</h4>
		<h4 class="main-header__settlement-subject">{{ $info->mo_subject_genitive }}</h4>
	</div>

	<div class="logo">
		<a href="/" class="main-header__logo">
			<img src="http://xn--b1aajldgc1acd1a8d.xn--p1ai/wp-content/themes/terra-sp-Vojskovickoe-selskoe-poselenie/img/sp-arms.png" alt="">
		</a>
	</div>

	<div class="main-header__nav-block">
		<a href="/">Главная</a>
		<a href="/pages/site-map">Карта сайта</a>
		<a href="/pages/request-in-electronic-form">Обращение (запрос) в электронном виде</a>
	</div>

	<form action="/search" method="get" class="main-header__form form">
		<input type="text" name="query" class="form__input">
		<button type="submit" class="form__button">Поиск</button>
	</form>

	<div class="address-block">
		<div class="address-block__zip">{{ $info->index }}, {{ $info->subject }}, {{ $info->district }},</div>
		<div class="address-block__city">{{ $info->settlement }}, {{ $info->address }}</div>
		<div class="address-block__phone">Телефон: {{ $info->phone }}, {{ $info->phone_extra }}</div>
		<div class="address-block__fax">{{ $info->fax }}</div>
		<div class="address-block__email">{{ $info->email }}</div>
	</div>

    {!! $hd->body !!}
</header>