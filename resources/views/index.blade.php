<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<title>{{$title}}</title>
    <link rel="stylesheet" href="/styles/style.css">
</head>
<body>
	<div class="container">
		{!! $header !!}
		<nav class="main-menu">
			<ul class="main-menu__container">
				@foreach($menu as $item)
					<li class="main-menu__item"><a class="main-menu__link" href="/pages/{{$item->slug}}">{{$item->title}}</a></li>
				@endforeach
			</ul>
		</nav>
        <div class="content">
        	{!! $content !!}
        </div>
    	{!! $footer !!}
	</div>
    <script src="/scripts/app.js"></script>
</body>
</html>