<footer class="main-footer">
	{!! $fu->body !!}


	<div class="footer__logo">
		<a href=""><img src="http://xn--b1aajldgc1acd1a8d.xn--p1ai/wp-content/themes/terra-sp-Vojskovickoe-selskoe-poselenie/img/sp-arms.png" alt=""></a>
	</div>

	<div class="footer__titles">
		<div class="footer__titles-title">{{ $info->title }}</div>
		<div class="footer__titles-settlement"><a href="/">{{ $info->mo_title_nominative }}</a></div>
		<div class="footer__titles-address">{{ $info->index }}, {{ $info->subject }}, {{ $info->district }}, {{ $info->settlement }}, {{ $info->address }}</div>
		<div class="footer__titles-contacts">Телефон: {{ $info->phone }}, {{ $info->phone_extra }}, факс: {{ $info->fax }}, email: {{ $info->email }}</div>
	</div>

	<div class="developer-info">
		<div class="developer-info__text">
			<div>Разработка и поддержка:</div>
			<a href="http://terra.spb.ru">ООО«Терра»</a> &copy; 2014
		</div>
	</div>

	{!! $fd->body !!}
	<div class="clear"></div>
</footer>