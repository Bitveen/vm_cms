CREATE TABLE `active_widgets` (
  `id` int(10) unsigned NOT NULL,
  `widget_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `row` int(10) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `active_widgets_options` (
  `id` int(10) unsigned NOT NULL,
  `active_widget_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `additional_content` (
  `type` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `additional_content` (`type`, `body`) VALUES
  ('fd', 'Здрасьте'),
  ('fu', 'Привет'),
  ('hd', 'конец хедера'),
  ('hu', 'Привет мир');
CREATE TABLE `main_info` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_title_nominative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_title_genitive` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_district_nominative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_district_genitive` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_subject_nominative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mo_subject_genitive` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `index` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settlement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_extra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `main_info` (`id`, `title`, `mo_title_nominative`, `mo_title_genitive`, `mo_district_nominative`, `mo_district_genitive`, `mo_subject_nominative`, `mo_subject_genitive`, `index`, `country`, `subject`, `district`, `settlement`, `address`, `phone`, `phone_extra`, `fax`, `email`) VALUES
  (1, 'Название сайта 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '891231283213', NULL, NULL, NULL);
CREATE TABLE `main_menu` (
  `id` int(10) unsigned NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `main_menu` (`id`, `is_active`, `order`, `page_id`) VALUES
  (1, 1, 1, 1),
  (2, 1, 2, 2),
  (3, 1, 3, 3),
  (4, 1, 4, 4);
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `pages` (`id`, `term_id`, `title`, `slug`) VALUES
  (1, 2, NULL, 'head-of-settlement'),
  (2, 3, NULL, 'administration'),
  (3, 4, NULL, 'council-of-deputies'),
  (4, 5, NULL, 'control-and-audit-authority'),
  (5, 6, NULL, 'search'),
  (6, 7, NULL, 'publications');
CREATE TABLE `publications` (
  `id` int(10) unsigned NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `pub_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `publications` (`id`, `title`, `content`, `pub_date`) VALUES
  (1, 'Первая публикация', 'Контент1', '2015-09-03'),
  (2, 'Вторая публикация', 'Контент', '2015-09-03'),
  (3, 'Третья публикация', 'Контент', '2015-09-03');
CREATE TABLE `publications_files` (
  `id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `terms` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `terms` (`id`, `title`, `description`) VALUES
  (1, 'Главная', NULL),
  (2, 'Глава поселения', NULL),
  (3, 'Администрация', NULL),
  (4, 'Совет депутатов', NULL),
  (5, 'Контрольно-счетный орган', NULL),
  (6, 'Поиск по сайту', NULL),
  (7, 'Все публикации', NULL),
  (10, 'Другая новая рубрика', NULL);
CREATE TABLE `terms_publications` (
  `term_id` int(10) unsigned NOT NULL,
  `publication_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `terms_relationships` (
  `term_id` int(10) unsigned NOT NULL,
  `parent_term_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `terms_relationships` (`term_id`, `parent_term_id`) VALUES
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 1),
  (6, 1),
  (7, 1),
  (10, 1);
CREATE TABLE `widgets` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `widgets_banners` (
  `id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widget_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `widgets_options` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `widget_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE `active_widgets`
ADD PRIMARY KEY (`id`),
ADD KEY `active_widgets_widget_id_foreign` (`widget_id`);
ALTER TABLE `active_widgets_options`
ADD PRIMARY KEY (`id`),
ADD KEY `active_widgets_options_active_widget_id_foreign` (`active_widget_id`),
ADD KEY `active_widgets_options_option_id_foreign` (`option_id`);
ALTER TABLE `additional_content`
ADD PRIMARY KEY (`type`);
ALTER TABLE `main_info`
ADD PRIMARY KEY (`id`);
ALTER TABLE `main_menu`
ADD PRIMARY KEY (`id`),
ADD KEY `main_menu_page_id_foreign` (`page_id`);
ALTER TABLE `pages`
ADD PRIMARY KEY (`id`),
ADD KEY `pages_term_id_foreign` (`term_id`);
ALTER TABLE `publications`
ADD PRIMARY KEY (`id`);
ALTER TABLE `publications_files`
ADD PRIMARY KEY (`id`),
ADD KEY `publications_files_publication_id_foreign` (`publication_id`);
ALTER TABLE `terms`
ADD PRIMARY KEY (`id`);
ALTER TABLE `terms_publications`
ADD KEY `terms_publications_term_id_foreign` (`term_id`),
ADD KEY `terms_publications_publication_id_foreign` (`publication_id`);
ALTER TABLE `terms_relationships`
ADD KEY `terms_relationships_term_id_foreign` (`term_id`),
ADD KEY `terms_relationships_parent_term_id_foreign` (`parent_term_id`);
ALTER TABLE `widgets`
ADD PRIMARY KEY (`id`);
ALTER TABLE `widgets_banners`
ADD PRIMARY KEY (`id`),
ADD KEY `widgets_banners_widget_id_foreign` (`widget_id`);
ALTER TABLE `widgets_options`
ADD PRIMARY KEY (`id`),
ADD KEY `widgets_options_widget_id_foreign` (`widget_id`);
ALTER TABLE `active_widgets`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `active_widgets_options`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `main_info`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `main_menu`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
ALTER TABLE `pages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `publications`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
ALTER TABLE `publications_files`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `terms`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
ALTER TABLE `widgets`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `widgets_banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `widgets_options`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `active_widgets`
ADD CONSTRAINT `active_widgets_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `widgets` (`id`) ON DELETE CASCADE;
ALTER TABLE `active_widgets_options`
ADD CONSTRAINT `active_widgets_options_active_widget_id_foreign` FOREIGN KEY (`active_widget_id`) REFERENCES `active_widgets` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `active_widgets_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `widgets_options` (`id`) ON DELETE CASCADE;
ALTER TABLE `main_menu`
ADD CONSTRAINT `main_menu_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;
ALTER TABLE `pages`
ADD CONSTRAINT `pages_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;
ALTER TABLE `publications_files`
ADD CONSTRAINT `publications_files_publication_id_foreign` FOREIGN KEY (`publication_id`) REFERENCES `publications` (`id`) ON DELETE CASCADE;
ALTER TABLE `terms_publications`
ADD CONSTRAINT `terms_publications_publication_id_foreign` FOREIGN KEY (`publication_id`) REFERENCES `publications` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `terms_publications_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;
ALTER TABLE `terms_relationships`
ADD CONSTRAINT `terms_relationships_parent_term_id_foreign` FOREIGN KEY (`parent_term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `terms_relationships_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;
ALTER TABLE `widgets_banners`
ADD CONSTRAINT `widgets_banners_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `active_widgets` (`id`) ON DELETE CASCADE;
ALTER TABLE `widgets_options`
ADD CONSTRAINT `widgets_options_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `widgets` (`id`) ON DELETE CASCADE;