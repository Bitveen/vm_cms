<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Script;

class ApiScriptsController extends Controller {

    public function getScriptData()
    {
        return response(file_get_contents(__DIR__.'/../../../../public/scripts/app.js'));
    }

    public function updateScriptData(Request $request)
    {
        return response(file_put_contents(__DIR__.'/../../../../public/scripts/app.js', $request->getContent()));
    }

}