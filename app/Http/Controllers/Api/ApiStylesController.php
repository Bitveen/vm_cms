<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Предоставляет интерфейс для взаимодействия со стилями сайта
 *
 * Class ApiStylesController
 * @package App\Http\Controllers
 */
class ApiStylesController extends Controller {

    /**
     * Метод для получения содержимого файла стилей
     *
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getStyle()
    {
        return response(file_get_contents(__DIR__.'/../../../../public/styles/style.css'));
    }

    /**
     * Метод для обновления содержимого файла стилей
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStyle(Request $request)
    {
        file_put_contents(__DIR__.'/../../../../public/styles/style.css', $request->getContent());
        return response()->json(['status' => 'updated']);
    }

}