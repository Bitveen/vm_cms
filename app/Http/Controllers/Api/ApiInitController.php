<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class ApiInitController extends Controller {
    public function init()
    {
        $sql = file_get_contents(base_path('storage').'/db.sql');
        $queries = explode(';', $sql);
        for ($i = 0; $i < count($queries); $i++) {
            if (strlen($queries[$i]) > 0 && (!empty($queries[$i]))) {
                DB::statement($queries[$i]);
            }
        }
    }
}
