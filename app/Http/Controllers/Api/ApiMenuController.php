<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;

/**
 * Предоставляет интерфейс для взаимодействия с меню сайта
 *
 * Class ApiMenuController
 * @package App\Http\Controllers\Api
 */
class ApiMenuController extends Controller {


    public function getMainMenuLinks()
    {
        $menu = Menu::all();
        return response()->json(compact('menu'));
    }


    public function update($menuId, Request $request)
    {
        if ($request->has('action')) {
            switch ($request->input('action')) {
                case 'remove':
                    return response(Menu::remove($menuId), 200);
                break;
                case 'change_order':
                    return response(Menu::changeOrder($menuId, json_decode($request->getContent())->order));
                break;

            }
        }
    }

    public function add(Request $request)
    {
        $data = json_decode($request->getContent());
        if (Menu::add($data->id, $data->order)) {
            return response('', 200);
        }
    }



}