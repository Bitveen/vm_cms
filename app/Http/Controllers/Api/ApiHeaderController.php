<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Header;
use App\Http\Controllers\Controller;

class ApiHeaderController extends Controller {

    public function getHeaderData()
    {
        return response(file_get_contents(__DIR__."/../../../../resources/views/header.blade.php"));
    }
    public function updateHeaderData(Request $request)
    {
        return response(file_put_contents(__DIR__.'/../../../../resources/views/header.blade.php', $request->getContent()));
    }


}