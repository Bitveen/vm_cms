<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Footer;
use App\Http\Controllers\Controller;

class ApiFooterController extends Controller {

    public function getFooterData()
    {
        return response(file_get_contents(__DIR__."/../../../../resources/views/footer.blade.php"));
    }

    
    public function updateFooterData(Request $request)
    {
        return response(file_put_contents(__DIR__.'/../../../../resources/views/footer.blade.php', $request->getContent()));
    }


}