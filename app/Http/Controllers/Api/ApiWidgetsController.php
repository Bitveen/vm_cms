<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Widget;
use App\Http\Controllers\Controller;

/**
 * Предоставляет интерфейс для взаимодействия с виджетами
 *
 * Class ApiWidgetsController
 * @package App\Http\Controllers
 */
class ApiWidgetsController extends Controller {

    public function getWidgets()
    {
        $widgets = Widget::all();
        return response()->json(compact('widgets'));
    }

}