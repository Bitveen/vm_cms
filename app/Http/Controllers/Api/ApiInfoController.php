<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Info;
use App\Http\Controllers\Controller;

/**
 * Предоставляет интерфейс для взаимодействия с основной информацией по сайту(поселение, контакты)
 *
 * Class ApiInfoController
 * @package App\Http\Controllers
 */
class ApiInfoController extends Controller {

    /**
     * Метод для получения информации о поселении
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getInfo()
    {
        return response()->json(Info::get());
    }


    /**
     * Метод для обновления информации о поселении
     *
     * @param Request $request
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateInfo(Request $request)
    {
        $content = json_decode($request->getContent());

        Info::update($content);

        return response()->json(['status' => 'ok']);

    }


}