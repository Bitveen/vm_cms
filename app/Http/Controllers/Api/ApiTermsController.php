<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Term;
use App\Http\Controllers\Controller;

/**
 * Предоставляет интерфейс для взаимодействия с рубриками сайта
 *
 * Class ApiTermsController
 * @package App\Http\Controllers
 */
class ApiTermsController extends Controller {

    public function getTerms()
    {
        return response()->json(Term::all());
    }



    public function updateTerm(Request $request)
    {
        $oldValue = $request->input('oldValue');
        $newValue = $request->input('newValue');
        if (Term::updateTitle($oldValue, $newValue)) {
            return response()->json(['status' => 'updated']);
        }

    }


    public function create(Request $request)
    {
        $title = $request->input('title');
        if (Term::create($title)) {
            return response()->json(['status' => 'created']);
        }

    }

    public function search(Request $request)
    {
        


        
    }


}