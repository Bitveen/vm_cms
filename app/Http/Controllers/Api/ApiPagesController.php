<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Page;
use App\Term;
use App\Http\Controllers\Controller;

/**
 * Предоставляет интерфейс для взаимодействия со страницами сайта
 *
 * Class ApiPagesController
 * @package App\Http\Controllers
 */
class ApiPagesController extends Controller {

    public function getPages(Request $request)
    {
        $pages = [];
        if ($request->has('type') && $request->input('type') == 'not_in_menu') {
            $pages = Page::allNotInMenu();
        } else {
            $pages = Page::all();
        }

        return response()->json($pages);
    }

    public function getPageData($pageId)
    {
        $page = Page::getPageById($pageId);
        $pageContent = file_get_contents(base_path('resources/views/pages').'/'.$page->slug.'.blade.php');

        return response()->json([
            'title' => $page->title,
            'slug' => $page->slug,
            'content' => $pageContent
        ]);

    }

    public function savePageData(Request $request, $pageId)
    {
        $page = Page::getPageById($pageId);
        $content = json_decode($request->getContent())->content;
        file_put_contents(base_path('resources/views/pages').'/'.$page->slug.'.blade.php', $content);
    }



    public function createPage(Request $request)
    {
        $data = json_decode($request->getContent());

        $content = $data->content;
        $title = $data->title;
        $slug = $data->slug;

        if (Term::exists($title) && !Term::notUsedInPages($title)) {
            return response()->json(['status' => 'term_exists_and_in_used'], 500);
        }


        if (Term::exists($title) && Term::notUsedInPages($title)) {
            // можно создавать страницу
            $termId = Term::getIdByTitle($title);
            if (Page::create($termId->id, $slug, $content)) {
                return response()->json(['status' => 'created']);
            }

        } else if (!Term::exists($title)) {
            // создать новую рубрику
            // создать страницу
            if ($termId = Term::create($title)) {
                // создать страницу
                if (Page::create($termId, $slug, $content)) {
                    return response()->json(['status' => 'created']);
                }
            }



        }


    }


}