<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Publication;
use App\Http\Controllers\Controller;

/**
 * Предоставляет интерфейс для взаимодействия с публикациями
 *
 * Class ApiPublicationsController
 * @package App\Http\Controllers
 */
class ApiPublicationsController extends Controller {

    public function getPublications()
    {
        return response()->json(Publication::all());
    }

    public function getPublicationData($publicationId)
    {
        return response()->json(Publication::get($publicationId));
    }

    public function updatePublication($publicationId, Request $request)
    {
        $data = json_decode($request->getContent());
        return response(Publication::update($publicationId, $data));
    }

    public function create(Request $request)
    {


        $publication = $request->only('title', 'content', 'pub_date');
        $files = json_decode($request->input('files'));

        if (Publication::create($publication, $files)) {
             return response('', 200);
        } else {
             return response('', 500);
        }
    }


}