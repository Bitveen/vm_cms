<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Layout;

/**
 * Предоставляет интерфейс для взаимодействия с разметкой сайта
 *
 * Class ApiLayoutController
 * @package App\Http\Controllers
 */
class ApiLayoutController extends Controller {

    /**
     * Метод для получения содержимого хедера и футера страницы
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getLayoutContent()
    {
        return response()->json([
            'header'    => file_get_contents(__DIR__."/../../../../resources/views/header.blade.php"),
            'footer'    => file_get_contents(__DIR__."/../../../../resources/views/footer.blade.php"),
            'container' => file_get_contents(__DIR__."/../../../../resources/views/index.blade.php")
        ]);
    }


    /**
     * Метод для обновления содержимого хедера и футера страницы
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateLayoutContent(Request $request)
    {
        if ($request->has('section')) {
            switch ($request->input('section')) {
                case 'header':
                    file_put_contents(__DIR__."/../../../../resources/views/header.blade.php", $request->getContent());
                    break;
                case 'footer':
                    file_put_contents(__DIR__."/../../../../resources/views/footer.blade.php", $request->getContent());
                    break;
                case 'container':
                    file_put_contents(__DIR__."/../../../../resources/views/index.blade.php", $request->getContent());
            }
        } else {
            return response()->json(['status' => 'section_not_provided'], 401);
        }
        return response()->json(['status' => 'ok']);
    }



    public function getAdditionalContent(Request $request)
    {
        return response(Layout::getAdditionalContent($request->input('type')));
    }


    public function setAdditionalContent(Request $request)
    {
        return response(Layout::setAdditionalContent($request->input('type'), $request->input('content')));
    }



}