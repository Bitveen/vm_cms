<?php namespace App\Http\Controllers;

use App\Menu;
use App\Term;
use Illuminate\Http\Request;
use App\Page;
use App\Widget;
use App\Info;
use App\Layout;
use App\Publication;

class SiteController extends Controller {

    private $info;

    private $menu = array();
    
    private $hu; // дополнительный контент для верха хедера
    private $hd; // дополнительный контент для низа хедера
    private $fu; // дополнительный контент для верха футера
    private $fd; // дополнительный контент для низа футера

    private $header;
    private $footer;


    public function __construct()
    {
        $this->info = Info::get();
        $this->menu = Menu::active();

        $this->hu = Layout::getAdditionalContent('hu');
        $this->hd = Layout::getAdditionalContent('hd');
        $this->fu = Layout::getAdditionalContent('fu');
        $this->fd = Layout::getAdditionalContent('fd');

        $this->header = view('header')->with([
            'info' => $this->info,
            'hu' => $this->hu,
            'hd' => $this->hd
        ]);

        $this->footer = view('footer')->with([
            'info' => $this->info,
            'fu' => $this->fu,
            'fd' => $this->fd
        ]);

    }

    /* Корень сайта */
    public function index()
    {
        $page = Page::getBySlug('main');
        $content = view('pages.main')->with([
            'page' => $page
        ]);

        return view('index')->with([
            'header' => $this->header,
            'title' => $this->info->title,
            'footer' => $this->footer,
            'content' => $content,
            'menu' => $this->menu
        ]);
    }

    /* Отдать страницу по slug */
    public function getPage($slug)
    {
        $page = Page::getBySlug($slug);
        $content = view('pages.'.$slug);


        return view('index')->with([
            'header' => $this->header,
            'title' => $this->info->title.' - '.$page->title,
            'footer' => $this->footer,
            'content' => $content,
            'menu' => $this->menu
        ]);
    }

    /* Отдать все публикации по данной категории */
    public function category($id)
    {

        $publications = Publication::allByCategory($id);
        //$terms = Term::allChildren($id);

        $content = view('pages.category')->with([
            'publications' => $publications
            //'term' => $term
        ]);

        return view('index')->with([
            'header' => $this->header,
            'title' => $term->title,
            'footer' => $this->footer,
            'content' => $content,
            'menu' => $this->menu
        ]);

    }

    /* Отдать публикацию по id */
    public function getPublication($id)
    {

        $publication = Publication::get($id);
        $content = view('pages.publication')->with([
            'publication' => $publication
        ]);

        return view('index')->with([
            'header'  => $this->header,
            'title'   => $publication->title,
            'footer'  => $this->footer,
            'content' => $content,
            'menu'    => $this->menu
        ]);
    }

    /* Поиск по публикациям */
    public function search(Request $request)
    {
        if ($request->has('query')) {
            $result = Publication::search($request->input('query'));

            $content = view('pages.search-result')->with([
                'publications' => $result
            ]);

            return view('index')->with([
                'header' => $this->header,
                'title' => 'Поиск по сайту',
                'footer' => $this->footer,
                'content' => $content,
                'menu' => $this->menu
            ]);

        } 
    }



}
