<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class HandleCORS {


    protected function setHeaders($request, $response)
    {
        if ($request->isMethod('OPTIONS')) {
            $response->header('Access-Control-Allow-Origin', '*');
            $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
            $response->header('Access-Control-Allow-Headers', 'Content-Type');
        } else {
            $response->header('Access-Control-Allow-Origin', '*');
        }


    }

    public function handle(Request $request, Closure $next)
    {

    	if ($request->isMethod('OPTIONS')) {

    		$response = new Response("", 200);

    	} else {

    		$response = $next($request);

    	}
        /*$response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        $response->header('Access-Control-Allow-Headers', 'Content-Type, Accept');*/


        $this->setHeaders($request, $response);
    	return $response;

    }


}
