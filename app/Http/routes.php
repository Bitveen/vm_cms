<?php

/* Роуты для самого сайта */

$app->get('/publications/{id}', 'SiteController@getPublication');
$app->get('/category/{id}', 'SiteController@category');
$app->get('/pages/{slug}', 'SiteController@getPage');

$app->get('/search', 'SiteController@search');
$app->get('/', 'SiteController@index');

/* Конец роутов для самого сайта */



/* Роуты для API */
$app->get('/api/info', 'Api\ApiInfoController@getInfo');
$app->post('/api/info', 'Api\ApiInfoController@updateInfo');


$app->get('/api/style', 'Api\ApiStylesController@getStyle');
$app->post('/api/style', 'Api\ApiStylesController@updateStyle');


$app->get('/api/layout', 'Api\ApiLayoutController@getLayoutContent');
$app->post('/api/layout', 'Api\ApiLayoutController@updateLayoutContent');


$app->get('/api/pages', 'Api\ApiPagesController@getPages');
$app->post('/api/pages', 'Api\ApiPagesController@createPage');
$app->get('/api/pages/{pageId}', 'Api\ApiPagesController@getPageData');
$app->post('/api/pages/{pageId}', 'Api\ApiPagesController@savePageData');



$app->get('/api/main_menu', 'Api\ApiMenuController@getMainMenuLinks');
$app->post('/api/main_menu', 'Api\ApiMenuController@add');
$app->post('/api/main_menu/{menuId}', 'Api\ApiMenuController@update');


$app->get('/api/additional_content', 'Api\ApiLayoutController@getAdditionalContent');
$app->post('/api/additional_content', 'Api\ApiLayoutController@setAdditionalContent');


$app->get('/api/widgets', 'Api\ApiWidgetsController@getWidgets');

$app->get('/api/publications', 'Api\ApiPublicationsController@getPublications');
$app->get('/api/publications/{publicationId}', 'Api\ApiPublicationsController@getPublicationData');

$app->post('/api/publications', 'Api\ApiPublicationsController@create');

$app->post('/api/publications/{publicationId}', 'Api\ApiPublicationsController@updatePublication');


$app->get('/api/terms', 'Api\ApiTermsController@getTerms');
$app->get('/api/terms/search', 'Api\ApiTermsController@search');

$app->post('/api/terms', 'Api\ApiTermsController@updateTerm');
$app->post('/api/terms/create', 'Api\ApiTermsController@create');



$app->post('/api/init', 'Api\ApiInitController@init');



$app->get('/api/scripts', 'Api\ApiScriptsController@getScriptData');
$app->post('/api/scripts', 'Api\ApiScriptsController@updateScriptData');

$app->get('/api/footer', 'Api\ApiFooterController@getFooterData');
$app->post('/api/footer', 'Api\ApiFooterController@updateFooterData');

$app->get('/api/header', 'Api\ApiHeaderController@getHeaderData');
$app->post('/api/header', 'Api\ApiHeaderController@updateHeaderData');




/* Конец роутов для API */