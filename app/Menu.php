<?php namespace App;

use DB;

class Menu {

    public static function active()
    {
        return DB::table('main_menu')
            ->join('pages', 'pages.id', '=', 'main_menu.page_id')
            ->select('pages.slug', 'pages.title')
            ->orderBy('main_menu.order', 'asc')
            ->where('main_menu.is_active', '=', true)
            ->get();
    }

    public static function all()
    {
        return DB::table('main_menu')
            ->join('pages', 'pages.id', '=', 'main_menu.page_id')
            ->select('main_menu.id', 'pages.title', 'main_menu.order')
            ->get();
    }

    public static function update($isActive, $menuId)
    {

        return DB::table('main_menu')->where('id', '=', $menuId)->update([
            'is_active' => $isActive == 'true' ? 1 : 0
        ]);
    }

    public static function remove($id)
    {
        return DB::table('main_menu')->where('id', '=', $id)->delete();
    }

    public static function add($pageId, $order)
    {
        return DB::table('main_menu')->insert([
            'is_active' => 1,
            'order' => $order,
            'page_id' => $pageId
        ]);
    }

    public static function changeOrder($id, $newOrder)
    {
        return DB::table('main_menu')->where('id', '=', $id)->update([
            'order' => $newOrder
        ]);
    }




}