<?php namespace App;

use DB;

class Term {


    public static function updateTitle($oldTitle, $newTitle)
    {
        $result = DB::table('terms')->where('title', '=', $oldTitle)->update([
            'title' => $newTitle
        ]);

        if ($result) {
            return true;
        }

        return false;
    }


    public static function all()
    {
        return DB::table('terms')
            ->select('*')
            ->orderBy('title')->get();
    }

    public static function allChildren($id)
    {
        
    }


    public static function create($termTitle, $parentTermId = 1)
    {

        if (!self::exists($termTitle)) {
            $id = DB::table('terms')->insertGetId([
                'title' => $termTitle
            ]);
            DB::table('terms_relationships')->insert([
                'term_id' => $id,
                'parent_term_id' => $parentTermId
            ]);
            return $id;
        }
    }

    public static function exists($title)
    {
        $count = DB::table('terms')->where('title', '=', $title)->count();
        if ($count) {
            return true;
        }
        return false;

    }

    public static function notUsedInPages($title)
    {
       if (!DB::table('terms')
           ->join('pages', 'pages.term_id', '=', 'terms.id')
           ->select('terms.title', 'terms.id')
           ->where('terms.title', '=', $title)->get()) {

           return true;

       }

        return false;

    }


    public static function getIdByTitle($title)
    {
        return DB::table('terms')->select('id')->where('title', '=', $title)->get()[0];
    }

    public static function getTitleById($termId)
    {
        return DB::table('terms')->where('id', '=', $termId)->select('title')->get()[0];
    }

}