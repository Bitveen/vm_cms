<?php namespace App;

use DB;

class Layout {

    public static function getAdditionalContent($type)
    {
        return DB::table('additional_content')->select('body', 'type')->where('type', '=', $type)->get()[0];
    }

    public static function setAdditionalContent($type, $body)
    {
        return DB::table('additional_content')->where('type', '=', $type)->update([
            'body' => $body
        ]);
    }




}