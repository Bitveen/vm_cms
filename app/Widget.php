<?php namespace App;

use DB;

class Widget {


	/**
	 * Получить все доступные виджеты
	 * @return mixed
     */
	public static function all()
	{
		return DB::table('widgets')->select('id', 'name')->get();
	}

	public static function allActive()
	{
        /*return DB::table('active_widgets')
            ->join('widgets', 'widgets.id', '=', 'active_widgets.widget_id')
            ->join('');*/
	}


	public static function activeById($widgetId)
	{
		$options = DB::table('widgets_options')->join('active_widgets_options', 'widgets_options.id', '=', 'active_widgets_options.option_id')
		->select('widgets_options.title', 'active_widgets_options.value')->where('active_widgets_options.active_widget_id', '=', $widgetId)->get();
		
		$widget = DB::table('active_widgets')->join('widgets', 'widgets.id', '=', 'active_widgets.widget_id')
		->select('widgets.content', 'active_widgets.title', 'active_widgets.row', 'active_widgets.position')->where('active_widgets.id', '=', $widgetId)->get()[0];
	
		return compact('widget', 'options');
	}

}