<?php namespace App;

use DB;

class Page {


	public static function all()
	{
        return DB::table('pages')->select('title', 'id', 'slug')->get();
	}

	public static function getById($pageId)
	{
		return DB::table('pages')->select('*')->where('id', '=', $pageId)->get()[0];
	}

	public static function getBySlug($pageSlug)
	{
		return DB::table('pages')
            ->join('terms', 'terms.id', '=', 'pages.term_id')
            ->select('terms.title', 'pages.id', 'pages.slug')->where('slug', '=', $pageSlug)->get()[0];
	}

	public static function getPageById($pageId)
	{
		return DB::table('pages')
            ->select('title', 'slug')
            ->where('id', '=', $pageId)
            ->get()[0];
	}




	public static function create($termId, $slug, $content)
    {
        if (DB::table('pages')->insert([
            'term_id' => $termId,
            'slug' => $slug
        ])) {
            // создать файл
            $f = fopen(__DIR__.'/../resources/views/pages/'.$slug.'.blade.php', 'w');
            if (fputs($f, $content)) {
                return true;
            }
            fclose($f);

        }
        return false;
    }

    public static function allNotInMenu()
    {
        return DB::select('select pages.id, terms.title, terms.id as term_id, pages.slug from pages inner join terms on pages.term_id = terms.id where pages.id not in (select page_id from main_menu)');
    }



}