<?php namespace App;

use DB;

class Info {

    public static function get()
    {
        return DB::table('main_info')->select('*')->get()[0];
    }

    public static function update($data)
    {
        return DB::table('main_info')->where('id', '=', 1)->update([
            'title' => $data->title,
            'mo_title_nominative' => $data->mo_title_nominative,
            'mo_title_genitive' => $data->mo_title_genitive,
            'mo_district_nominative' => $data->mo_district_nominative,
            'mo_district_genitive' => $data->mo_district_genitive,
            'mo_subject_nominative' => $data->mo_subject_nominative,
            'mo_subject_genitive' => $data->mo_subject_genitive,
            'index' => $data->index,
            'country' => $data->country,
            'subject' => $data->subject,
            'district' => $data->district,
            'settlement' => $data->settlement,
            'address' => $data->address,
            'phone' => $data->phone,
            'phone_extra' => $data->phone_extra,
            'fax' => $data->fax,
            'email' => $data->email
        ]);
    }


}