<?php namespace App;

use DB;

class Publication {

    public static function all()
    {
        return DB::table('publications')->select('*')->get();
    }

    public static function get($publicationId)
    {
        return DB::table('publications')->select('*')->where('id', '=', $publicationId)->get()[0];
    }

    public static function allByCategory($categoryId)
    {
        return DB::table('publications')
            ->join('terms_publications', 'publications.id', '=', 'terms_publications.publication_id')
            ->select('publications.title', 'publications.content', 'publications.pub_date', 'publications.id')
            ->where('terms_publications.term_id', '=', $categoryId)
            ->get();
    }

    public static function update($publicationId, $data)
    {
        return DB::table('publications')->where('id', '=', $publicationId)
            ->update([
                'title' => $data->title,
                'content' => $data->content,
                'pub_date' => $data->pub_date
            ]);
    }

    public static function search($query)
    {
        return DB::table('publications')
            ->where('content', 'like', '%'.$query.'%')
            ->select('*')->get();
    }

    public static function create($publication, $files)
    {
        $id = DB::table('publications')->insertGetId($publication);

        $filesQuery = "INSERT INTO publications_files(url, name, type, publication_id) VALUES ";
        foreach ($files as $file) {
            $filePath = $file->hash_path;
            $fileName = $file->name;
            $fileType = $file->mime_type;
            $filesQuery .= "('$filePath', '$fileName', '$fileType', '$id'), ";
        }
        $filesQuery = mb_substr($filesQuery, 0, mb_strlen($filesQuery) - 2);
        return DB::insert($filesQuery);
    }



}