<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('mo_title_nominative')->nullable();
            $table->string('mo_title_genitive')->nullable();
            $table->string('mo_district_nominative')->nullable();
            $table->string('mo_district_genitive')->nullable();
            $table->string('mo_subject_nominative')->nullable();
            $table->string('mo_subject_genitive')->nullable();
            $table->string('index')->nullable();
            $table->string('country')->nullable();
            $table->string('subject')->nullable();
            $table->string('district')->nullable();
            $table->string('settlement')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_extra')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main_info');
    }
}
