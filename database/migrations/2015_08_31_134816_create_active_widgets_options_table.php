<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveWidgetsOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_widgets_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('active_widget_id')->unsigned();
            $table->integer('option_id')->unsigned();
            $table->text('value')->nullable();
            $table->foreign('active_widget_id')->references('id')->on('active_widgets')->onDelete('cascade');
            $table->foreign('option_id')->references('id')->on('widgets_options')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('active_widgets_options');
    }
}
